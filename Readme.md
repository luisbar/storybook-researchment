## What is this?
A monorepo which contains useful examples about Storybook tool.

### 1. Tutorial
A package which contains code that will gives you an idea how Storybook works.

You can run executing the following sentence:
```
npm run -w tutorial storybook
```

Open [http://192.168.1.111:6006](http://192.168.1.111:6006/) instead of [http://localhost:6006/](http://localhost:6006/), because MSW addon won't work with `localhost`

#### Todos

- [ ] Visual test with Storyshots addon
- [ ] Snapshot test with Storyshots addon
- [ ] Visual test with Chromatic
- [ ] Package composition