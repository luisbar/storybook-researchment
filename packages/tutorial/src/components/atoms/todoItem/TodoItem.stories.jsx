import React from 'react';

import { TodoItem } from './TodoItem';

export default {
  title: 'Atoms/TodoItem',
  component: TodoItem,
};

const Template = (args) => <TodoItem {...args} />;

export const Checked = Template.bind({});
Checked.args = {
  id: 1,
  checked: true,
  label: 'Todo 1',
};

export const Unchecked = Template.bind({});
Unchecked.args = {
  id: 2,
  label: 'Todo 2',
};
