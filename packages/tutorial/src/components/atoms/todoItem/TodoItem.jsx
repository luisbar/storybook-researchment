import React from 'react';
import PropTypes from 'prop-types';
import './todoItem.css';

/**
 * Primary UI component for displaying todos
 */
export const TodoItem = ({ id, checked, label }) => {
  return (
    <div
      className='container'
    >
      <input
        className='checkbox'
        id={`todo-${id}`}
        type={'checkbox'}
        checked={checked}
      />
      <label
        for={`todo-${id}`}
      >
        {label}
      </label>
    </div>
  );
};

TodoItem.propTypes = {
  /**
   * Todo id
   */
  id: PropTypes.string.isRequired,
  /**
   * should the checkbox be checked?
   */
  checked: PropTypes.bool,
  /**
   * Description of the todo
   */
  label: PropTypes.string,
};

TodoItem.defaultProps = {
  checked: false,
  label: '?',
};
