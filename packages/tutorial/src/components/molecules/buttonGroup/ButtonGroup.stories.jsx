// ButtonGroup.stories.js|jsx

import React from 'react';

import { ButtonGroup } from './ButtonGroup';

//👇 Imports the Button stories
import * as ButtonStories from '../../atoms/button/Button.stories';

export default {
  /* 👇 The title prop is optional.
  * See https://storybook.js.org/docs/react/configure/overview#configure-story-loading
  * to learn how to generate automatic titles
  */
  title: 'Molecules/ButtonGroup',
  component: ButtonGroup,
};

const Template = (args) => <ButtonGroup {...args} />;

export const Horizontal = Template.bind({});
Horizontal.args = {
  buttons: [
    { ...ButtonStories.Secondary.args },
    { ...ButtonStories.Primary.args },
    { ...ButtonStories.Primary.args },
  ],
  orientation: 'horizontal',
};

export const Vertical = Template.bind({});
Vertical.args = {
  buttons: [
    { ...ButtonStories.Secondary.args },
    { ...ButtonStories.Primary.args },
    { ...ButtonStories.Primary.args },
  ],
  orientation: 'vertical',
};