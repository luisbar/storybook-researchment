import React from 'react';
import PropTypes from 'prop-types';
import './buttonGroup.css';
import { Button } from '../../atoms/button/Button';

/**
 * Grouped buttons
 */
export const ButtonGroup = ({ buttons, orientation }) => {
  return (
    <div
      className={orientation} 
    >
      {buttons.map((props) => <Button {...props} />)}
    </div>
  );
};

ButtonGroup.propTypes = {
  /**
   * Buttons to show
   */
  buttons: PropTypes.arrayOf(Button),
  /**
   * What orientation the buttons should have?
   */
  orientation: PropTypes.oneOf(['vertical', 'horizontal']),
};

ButtonGroup.defaultProps = {
  buttons: [],
  orientation: 'vertical',
};
