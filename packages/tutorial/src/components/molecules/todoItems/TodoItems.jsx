import React from 'react';
import PropTypes from 'prop-types';
import './todoItems.css';
import { TodoItem } from '../../atoms/todoItem/TodoItem';

/**
 * Grouped todoItems
 */
export const TodoItems = ({ todoItems }) => {
  return (
    <div
      className='vertical' 
    >
      {todoItems.map((props) => <TodoItem {...props} />)}
    </div>
  );
};

TodoItems.propTypes = {
  /**
   * TodoItems to show
   */
  todoItems: PropTypes.arrayOf(TodoItem),
};

TodoItems.defaultProps = {
  todoItems: [],
};
