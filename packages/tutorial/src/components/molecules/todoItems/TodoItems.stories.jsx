// TodoItems.stories.js|jsx

import React from 'react';

import { TodoItems } from './TodoItems';

export default {
  /* 👇 The title prop is optional.
  * See https://storybook.js.org/docs/react/configure/overview#configure-story-loading
  * to learn how to generate automatic titles
  */
  title: 'Molecules/TodoItems',
  component: TodoItems,
};

const fakeFetch = () => {
  return new Promise((resolve) => {
    resolve([...Object.keys(Array.from({ length: 10 }))].map((id) => ({
      id,
      label: `Todo ${id}`,
      checked: Math.floor(Math.random() * 2),
    })))
  });
}

const Template = (args, { loaded: { todos } }) => <TodoItems todoItems={todos} />;

export const Normal = Template.bind({});
Normal.loaders = [
  async () => ({
    todos: await fakeFetch(),
  }),
]