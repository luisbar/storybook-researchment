import React from 'react';
import { within, userEvent } from '@storybook/testing-library';
import { expect } from '@storybook/jest';
import { rest } from 'msw';

import { Home } from './Home';

export default {
  title: 'Pages/Home',
  component: Home,
  parameters: {
    // More on Story layout: https://storybook.js.org/docs/react/configure/story-layout
    layout: 'fullscreen',
  },
};

const Template = (args) => <Home  {...args} />;

// More on interaction testing: https://storybook.js.org/docs/react/writing-tests/interaction-testing
export const LoggedOut = Template.bind({});
LoggedOut.play = async ({ canvasElement }) => {
  await LoggedIn.play({ canvasElement });
  const canvas = within(canvasElement);

  const logoutButton = await canvas.getByRole('button', { name: /Log out/i });
  await userEvent.click(logoutButton);

  const loginButton = await canvas.getByRole('button', { name: /Log in/i });
  await expect(loginButton).toBeInTheDocument();
};

export const LoggedIn = Template.bind({});
LoggedIn.play = async ({ canvasElement }) => {
  const canvas = within(canvasElement);
  const loginButton = await canvas.getByRole('button', { name: /Log in/i });
  await userEvent.click(loginButton);

  const logoutButton = await canvas.getByRole('button', { name: /Log out/i });
  expect(logoutButton).toBeInTheDocument();
};

LoggedIn.parameters = {
  msw: {
    handlers: [
      rest.get('http://localhost:7001', (_req, res, ctx) => {
        return res(ctx.json([...Object.keys(Array.from({ length: 10 }))].map((id) => ({
          id,
          label: `Todo ${id}`,
          checked: Math.floor(Math.random() * 2),
        }))));
      }),
    ]
  }
};
