import React, { useEffect, useState } from 'react';

import { Header } from '../../organisms/header/Header';
import { TodoItems } from '../../molecules/todoItems/TodoItems';
import './home.css';

export const Home = () => {
  const [user, setUser] = useState();
  const [todos, setTodos] = useState([])

  useEffect(() => {
    fetch('http://localhost:7001', {
      method: 'GET'
    })
    .then((response) => response.json())
    .then((result) => setTodos(result))
    .catch((er) => console.log(er))
  }, []);

  return (
    <article>
      <Header
        user={user}
        onLogin={() => setUser({ name: 'Jane Doe' })}
        onLogout={() => setUser(undefined)}
        onCreateAccount={() => setUser({ name: 'Jane Doe' })}
      />
      { user &&
        <section>
          <h2>Todos</h2>
          <br/>
          <br/>
          <TodoItems
            todoItems={todos}
          />
        </section>
      }
    </article>
  );
};
