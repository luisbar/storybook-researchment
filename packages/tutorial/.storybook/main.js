module.exports = {
  "stories": [
    "../overview.stories.mdx",
    "../changelog.stories.mdx",
    "../src/components/atoms/**/*.stories.@(js|jsx|ts|tsx|mdx)",
    "../src/components/molecules/**/*.stories.@(js|jsx|ts|tsx|mdx)",
    "../src/components/organisms/**/*.stories.@(js|jsx|ts|tsx|mdx)",
    "../src/components/templates/**/*.stories.@(js|jsx|ts|tsx|mdx)",
    "../src/components/pages/**/*.stories.@(js|jsx|ts|tsx|mdx)",
  ],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions",
    "@storybook/addon-a11y"
  ],
  "framework": "@storybook/react",
  "features": {
    "interactionsDebugger": true,
  },
}